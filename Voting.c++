#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair
#include "Voting.h"

using namespace std;

bool Candidate::isEliminated() const {
    return eliminated;
}

int Ballot::getCandidate() {
    assert(index < (int)votes.size());
    return votes[index];
}

void Candidate::setEliminated() {
    eliminated = true;
}

int Ballot::moveToNext(const vector<Candidate>& cl) {
    ++index;
    while (cl[getCandidate() - 1].isEliminated()) {
        ++index;
    }
    return getCandidate();
}

int getValidCount(vector<Candidate>& candidateList) {
    int validCandidates = 0;
    for(int i = 0; i < (int)candidateList.size(); ++i) {
        if(!candidateList[i].isEliminated()) {
            ++validCandidates;
        }
    }
    assert(validCandidates > 0);
    return validCandidates;
}

// More than 50% == win
// All candidates have the same number of votes == tie
// Otherwise, there will be 1 or more losers to be eliminated
bool electionRound(vector<Candidate>& candidateList, int numBallots, ostream& w) {
    int majority = numBallots / 2 + 1;
    vector<Candidate> loseList;
    int lowestVotes = 1001;
    for(int i = 0; i < (int)candidateList.size(); ++i) {
        if(!candidateList[i].isEliminated()) {
            if(candidateList[i].voteCount() >= majority) {
                w << candidateList[i].getName() << '\n';
                return true;
            }
            if(candidateList[i].voteCount() <= lowestVotes) {
                if(candidateList[i].voteCount() < lowestVotes) { // new loser is found
                    loseList.clear();
                    lowestVotes = candidateList[i].voteCount();
                }
                loseList.push_back(candidateList[i]);
            }
        }
    }
    if((int)loseList.size() == getValidCount(candidateList)) {  // if every candidate is in the loser list,
        for(int i = 0; i < (int)loseList.size(); ++i) {         // there is an exact tie
            w << loseList[i].getName() << '\n';
        }
        return true;
    } else {
        for (int i = 0; i < (int)loseList.size(); ++i) {         // set eliminated flag for all losers
            int index = loseList[i].getID() - 1;
            assert(index < (int)candidateList.size());
            candidateList[index].setEliminated();
        }
        for (int i = 0; i < (int)loseList.size(); ++i) {          // reassign ballots for all of the losers
            vector<Ballot>& ballots = loseList[i].getBallots();
            for (int j = 0; j < (int)ballots.size(); ++j) {
                candidateList[ballots[j].moveToNext(candidateList) - 1].addBallot(ballots[j]);
            }
            ballots.clear();
        }
    }
    return false;
}

void votingSolve(istream& r, ostream& w) {
    int cases;
    r >> cases;
    assert(cases > 0);
    assert(cases <= 100);
    while (cases-- > 0) {
        vector<Candidate> candidateList;
        int numCandidates;
        r >> numCandidates;
        assert(numCandidates > 0);
        assert(numCandidates <= 20);
        {   // get rid of empty input line
            string garbage;
            getline(r, garbage);
        }

        for (int i = 0; i < numCandidates; ++i) {   // Read in candidates
            string name;
            getline(r, name);
            Candidate can(name, i + 1);
            candidateList.push_back(can);
        }
        string line;
        getline(r, line);
        int numBallots = 0;
        while (line != "") {            // read an indefinite number of ballots
            ++numBallots;
            vector<int> ballot;
            istringstream sin(line);
            Ballot b(sin, numCandidates);
            candidateList[b.getCandidate() - 1].addBallot(b);
            getline(r, line);
        }
        assert(numBallots > 0);
        assert(numBallots <= 1000);
        while(!electionRound(candidateList, numBallots, w));  // keep eliminating candidates until winner or tie is found
        if(cases != 0) {
            w << "\n";
        }
    }
}
