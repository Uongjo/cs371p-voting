// --------------
// RunVoting.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include "Voting.h"

// ----
// main
// ----

int main () {
    using namespace std;
    votingSolve(cin, cout);
    return 0;
}
