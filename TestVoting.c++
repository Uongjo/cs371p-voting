// ---------------
// TestVoting.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string
#include <utility>  // pair

#include "gtest/gtest.h"

#include "Voting.h"

using namespace std;

// -----------
// TestVoting
// -----------

TEST(CandidateConstructor, candidate_1) {
    string s("Joel Uong");
    Candidate c(s, 1);
    ASSERT_TRUE(!c.isEliminated());
    ASSERT_EQ(1, c.getID());
    ASSERT_EQ("Joel Uong", c.getName());
}


TEST(BallotPlacement, ballot_1) {
    string s("Joel Uong");
    Candidate c(s, 1);
    istringstream str("1 2 3");
    Ballot b(str, 3);
    c.addBallot(b);
    ASSERT_EQ(c.voteCount(), 1);
}

TEST(EliminateCandidate, candidate_2) {
    Candidate c("Carson Ham", 1);
    ASSERT_TRUE(!c.isEliminated());
    c.setEliminated();
    ASSERT_TRUE(c.isEliminated());
}

TEST(TieCase, voting_1) {
    istringstream r("1\n\n2\nCarson Ham\nJoel Uong\n1 2\n2 1\n");
    ostringstream w;
    votingSolve(r, w);
    ASSERT_EQ(w.str(), "Carson Ham\nJoel Uong\n");
}

TEST(WinCase, voting_2) {
    istringstream r("1\n\n4\nNeha Shah\nJoel Uong\nCarson Ham\nRey Eisen\n1 2 3 4\n1 2 3 4\n2 1 4 3\n2 1 4 3\n3 1 2 4\n4 1 2 3\n");
    ostringstream w;
    votingSolve(r, w);
    ASSERT_EQ(w.str(), "Neha Shah\n");
}

TEST(MoveToNext, ballot_2) {
    vector<Candidate> cl;
    cl.push_back(Candidate("Joel Uong", 1));
    cl.push_back(Candidate("Carson Ham", 2));
    cl.push_back(Candidate("Neha Shah", 3));
    cl[1].setEliminated();
    istringstream s("1 2 3");
    Ballot b(s, 3);
    ASSERT_EQ(b.moveToNext(cl), 3);
}

TEST(WinRound, election_round_1) {
    vector<Candidate> cl;
    cl.push_back(Candidate("Joel Uong", 1));
    cl.push_back(Candidate("Carson Ham", 2));
    cl.push_back(Candidate("Neha Shah", 3));
    istringstream s1("1 2 3");
    istringstream s2("1 2 3");
    istringstream s3("2 1 3");
    cl[0].addBallot(Ballot(s1, 3));
    cl[0].addBallot(Ballot(s2, 3));
    cl[1].addBallot(Ballot(s3, 3));
    ostringstream w;
    bool win = electionRound(cl, 3, w);
    ASSERT_EQ("Joel Uong\n", w.str());
    ASSERT_TRUE(win);
}

TEST(LoseRound, election_round_2) {
    vector<Candidate> cl;
    cl.push_back(Candidate("Joel Uong", 1));
    cl.push_back(Candidate("Carson Ham", 2));
    cl.push_back(Candidate("Neha Shah", 3));
    istringstream s1("1 2 3");
    istringstream s2("1 2 3");
    istringstream s3("2 1 3");
    istringstream s4("2 1 3");

    cl[0].addBallot(Ballot(s1, 3));
    cl[0].addBallot(Ballot(s2, 3));
    cl[1].addBallot(Ballot(s3, 3));
    cl[1].addBallot(Ballot(s4, 3));

    ostringstream w;
    bool win = electionRound(cl, 4, w);
    cout << w.str();
    ASSERT_EQ("", w.str());
    ASSERT_FALSE(win);
}

TEST(ValidCount, count_1) {
    vector<Candidate> cl;
    cl.push_back(Candidate("Joel Uong", 1));
    cl.push_back(Candidate("Carson Ham", 2));
    cl.push_back(Candidate("Neha Shah", 3));

    ASSERT_EQ(3, getValidCount(cl));
}

TEST(ValidCount2, count_2) {
    vector<Candidate> cl;
    cl.push_back(Candidate("Joel Uong", 1));
    cl.push_back(Candidate("Carson Ham", 2));
    cl.push_back(Candidate("Neha Shah", 3));
    cl[1].setEliminated();
    cl[2].setEliminated();

    ASSERT_EQ(1, getValidCount(cl));
}
