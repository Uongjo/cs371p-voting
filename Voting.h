// ---------
// Voting.h
// ---------

#ifndef Voting_h
#define Voting_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <string>   // string
#include <utility>  // pair
#include <vector>

using namespace std;

class Candidate;

class Ballot {
private:
    vector<int> votes;
    int index;
public:
    /**
     * Constructor for Ballot class
     * @param r an istream
     * @param numCandidates which is the total number of Candidates
     */
    Ballot(istream& r, int numCandidates) : votes(), index(0) {
        int v;
        for (int i = 0; i < numCandidates; ++i) {
            r >> v;
            votes.push_back(v);
        }
    }

    /**
     * @return returns number representing candidate that ballot is voting for
     */
    int getCandidate();

    /**
     *In the case where a candidate is elimnated, the vote goes to the next valid candidate
     * @param vector candidateList which contains all candidates
     * @return unique ID of current candidate ballot is voting for
     */
    int moveToNext(const vector<Candidate>& cl);
};


class Candidate {
private:
    bool eliminated;
    vector<Ballot> ballots;
    string name;
    int id;
public:
    /**
     * Constructor for Candidate class
     * @param name of the candidate
     * @param id which is the unique number representing the candidate
     */
    Candidate(string name, int id) :
        eliminated(false),
        ballots(),
        name(name),
        id(id) {}

    /**
     * @return returns true if candidate is still in the running, false otherwise
     */
    bool isEliminated() const;

    /**
     * sets eliminated flag
     */
    void setEliminated();

    /**
     * @return returns candidates unique number
     */
    int getID() {
        return id;
    }

    /**
     * @return returns string name
     */
    string getName() {
        return name;
    }

    /**
     * Adds ballot for a candidate if voted for
     * @param Ballot in favor for candidate
     */
    void addBallot(Ballot b) {
        ballots.push_back(b);
    }

    /**
     * @return returns number of current votes for a candidate
     */
    int voteCount() {
        return (int)ballots.size();
    }

    /**
     * @return returns ballots who are in favor of a candidate
     */
    vector<Ballot>& getBallots() {
        return ballots;
    }
};

/**
 * @param candidateList a vector of ALL candidates
 * @return returns number of candidates still in the running
 */
int getValidCount(vector<Candidate>& candidateList);

/**
 * Simulates an election round finding losers, winners, and ties
 * @param candidateList a vector of ALL candidates
 * @param numBallots number of total ballots in an election
 * @param w an ostream
 * @return true if we have found a winner/tie, false otherwise
 */
bool electionRound(vector<Candidate>& candidateList, int numBallots, ostream& w);

/**
 * @param r an istream
 * @param w an ostream
 */
void votingSolve(istream& r, ostream& w);

#endif // Voting_h
